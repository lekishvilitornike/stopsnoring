<!DOCTYPE html>
<!--[if IE 9 ]>
<html lang="en-US" class="ie9 loading-site no-js bg-fill"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en-US" class="ie8 loading-site no-js bg-fill"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US" class="loading-site no-js bg-fill"> <!--<![endif]-->

<?php include './components/head.php'; ?>

<body class="page-template-default page page-id-621 boxed bg-fill box-shadow lightbox nav-dropdown-has-arrow">

<a class="skip-link screen-reader-text" href="#main">Skip to content</a>

<div id="wrapper">
    <?php include "./components/header.php"; ?>
    <main id="main" class="">
        <div id="content" class="content-area page-wrapper" role="main">
            <div class="row row-main">
                <div class="large-12 col">
                    <div class="col-inner">
                        <p><strong>Do you need help?</strong><br>
                            In case you want to receive further information our award winning customer advisors will
                            offer you their help!</p>
                        <p><strong>Contact US</strong><br>
                            Contact our Customer service team Call: International: &#8237;+44 20 3808 9234, Brazil: +55
                            15 981471395<br>
                            Send an email to: support@hyperstech.com</p>
                        <p>Novads OU<br>
                            Narva mnt 7, 5th Floor<br>
                            Tallinn – 10117<br>
                            Estonia</p>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->
        </div>


    </main>
    <?php include "./components/footer.php"; ?>

</div><!-- #wrapper -->

<?php include './components/mobileFooter.php'; ?>

<?php include './components/scripts.php'; ?>
</body>
</html>
