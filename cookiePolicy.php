<!DOCTYPE html>
<!--[if IE 9 ]>
<html lang="en-US" class="ie9 loading-site no-js bg-fill"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en-US" class="ie8 loading-site no-js bg-fill"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US" class="loading-site no-js bg-fill"> <!--<![endif]-->
<?php include './components/head.php' ?>

<body class="page-template-default page page-id-142 boxed bg-fill box-shadow lightbox nav-dropdown-has-arrow">

<a class="skip-link screen-reader-text" href="#main">Skip to content</a>

<div id="wrapper">
    <?php include "./components/header.php"; ?>

    <main id="main" class="">
        <div id="content" class="content-area page-wrapper" role="main">
            <div class="row row-main">
                <div class="large-12 col">
                    <div class="col-inner">


                        <p>An HTTP cookie (also called web cookie, Internet cookie, browser cookie, or simply cookie) is
                            a small piece of data sent from a website and stored on the user’s computer by the user’s
                            web browser while the user is browsing.Cookies are designed to be a reliable mechanism for
                            websites to remember stateful information (such as items added in the shopping cart in an
                            online store) or to record the user’s browsing activity (including clicking particular
                            buttons, logging in, or recording which pages were visited in the past).</p>
                        <p>Our website uses cookies to improve our service for you. Some cookies we use are essential
                            for some services to work, others are used to collect information of the use of the website
                            (statistics) so that we can make the site more convenient and useful for you. Some cookies
                            are temporary and will disappear when you close your browser, others are persistent and will
                            stay on your computer for some time. We are also using some local cookies that are tied to
                            local campaigns and which will disappear when the campaign ends.<br>
                            Some of the cookies are strictly necessary for the functionality of the site while others
                            are used to enhance the performance and your user experience.</p>
                        <p>We suggest that you consult your browser “Help” section or read on <a
                                    href="https://www.aboutcookies.org/" target="_blank" rel="noopener"><span
                                        style="color: #0000ff;">the About Cookies website</span></a> which offers
                            instructions for all modern browsers.</p>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->
        </div>


    </main>

    <?php include "./components/footer.php"; ?>

</div><!-- #wrapper -->

<?php include "./components/mobileFooter.php"; ?>

<?php include "./components/scripts.php"; ?>
</body>
</html>
