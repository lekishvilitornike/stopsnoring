<!DOCTYPE html>
<!--[if IE 9 ]>
<html lang="en-US" class="ie9 loading-site no-js bg-fill"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en-US" class="ie8 loading-site no-js bg-fill"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US" class="loading-site no-js bg-fill"> <!--<![endif]-->

<?php include './components/head.php'; ?>

<body class="page-template-default page page-id-621 boxed bg-fill box-shadow lightbox nav-dropdown-has-arrow">

<a class="skip-link screen-reader-text" href="#main">Skip to content</a>

<div id="wrapper">
    <?php include "./components/header.php"; ?>
    <main id="main" class="">
        <div id="content" class="content-area page-wrapper" role="main">
            <div class="row row-main">
                <div class="large-12 col">
                    <div class="col-inner">
                        <p><strong>1. Name of the Company</strong><br/>
                            International: Hyper Sls Ltd<br/>
                            Europe: Novads OU</p>
                        <p><strong>2. Company Registration number</strong><br/>
                            2544183</p>
                        <p><strong>3. Email address</strong><br/>
                            <a href="/cdn-cgi/l/email-protection" class="__cf_email__"
                               data-cfemail="3e4d4b4e4e514c4a7e56474e5b4c4d4a5b5d56105d5153">[email&#160;protected]</a>
                            &#8211; customer support</p>
                        <p><strong>4. Firm telephone number</strong><br/>
                            General Question : +852 8197 7604<br/>
                            customer support number : +852 8197 7604</p>
                        <p><strong>5. Company name and address</strong><br/>
                            International Office:<br/>
                            Hyper Sls Ltd<br/>
                            7/F, The Grande Building<br/>
                            398-402 Kwun Tong Road, Kowloon, HongKong.</p>
                        <p><strong>Europe Office: </strong><br/>
                            Novads OU<br/>
                            Narva mnt 7, 5th Floor, room 556<br/>
                            Tallinn, Estonia, 10117</p>
                        <p><strong>6. Company Director</strong><br/>
                            Peter Novik</p>


                    </div><!-- .col-inner -->
                </div><!-- .large-12 -->
            </div><!-- .row -->
        </div>
    </main><!-- #main -->

    <?php include "./components/footer.php"; ?>

</div><!-- #wrapper -->

<?php include './components/mobileFooter.php'; ?>

<?php include './components/scripts.php'; ?>
</body>
</html>
