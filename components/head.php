<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <link rel="pingback" href="https://stopsnoring.silent-snore.shop/xmlrpc.php"/>

    <script>(function (html) {
            html.className = html.className.replace(/\bno-js\b/, 'js')
        })(document.documentElement);</script>
    <title>SilentSnore ® &#8211; Official Website</title>
    <link rel="alternate" type="application/rss+xml" title="SilentSnore ® &raquo; Feed"
          href="https://example.com/feed/"/>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'
          href='./assets/css/style.min.css?ver=5.2.4'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='./assets/css/styles.css?ver=4.9'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wpfomo-css'
          href='./assets/css/wpfomo-public.css?ver=1.1.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='flatsome-icons-css'
          href='./assets/css/fl-icons.css?ver=3.3'
          type='text/css' media='all'/>
    <link rel="stylesheet" type="text/css" href="./assets/css/flatsome.css">
    <link rel='stylesheet' id='flatsome-style-css'
          href='./assets/css/flatsome/style.css?ver=3.6.2' type='text/css'
          media='all'/>
    <script type='text/javascript'
            src='./assets/js/jquery/jquery.js?ver=1.12.4-wp'></script>
    <script type='text/javascript'
            src='./assets/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript'
            src='./assets/js/wpfomo-public.js?ver=1.1.0'></script>

    <link rel="canonical" href="https://example.com/"/>
    <link rel='shortlink' href='https://example.com/'/>

    <style>
        .bg {
            opacity: 0;
            transition: opacity 1s;
            -webkit-transition: opacity 1s;
        }

        .bg-loaded {
            opacity: 1;
        }</style>
    <!--[if IE]>
    <link rel="stylesheet" type="text/css"
          href="https://stopsnoring.silent-snore.shop/wp-content/themes/flatsome/assets/css/ie-fallback.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
    <script>var head = document.getElementsByTagName('head')[0], style = document.createElement('style');
    style.type = 'text/css';
    style.styleSheet.cssText = ':before,:after{content:none !important';
    head.appendChild(style);
    setTimeout(function () {
        head.removeChild(style);
    }, 0);</script>
    <script src="./assets/js/ie-flexibility.js"></script>
    <![endif]-->
    <script type="text/javascript">
        WebFontConfig = {
            google: {families: ["Open+Sans:regular,regular", "Open+Sans:regular,regular", "Poppins:regular,700", "Noto+Sans:regular,regular",]}
        };
        (function () {
            var wf = document.createElement('script');
            wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })(); </script>
    <link rel="icon"
          href="./assets/imgs/cropped-fav-silent-snore-32x32.png"
          sizes="32x32"/>
    <link rel="icon"
          href="./assets/imgs/cropped-fav-silent-snore-192x192.png"
          sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed"
          href="./assets/imgs/cropped-fav-silent-snore-180x180.png"/>
    <meta name="msapplication-TileImage"
          content="./assets/imgs/cropped-fav-silent-snore-270x270.png"/>

    <style type="text/css" id="wp-custom-css">
        .header {
            display: none;
        }

        :root {
            --primary-color: #82b14e;
        }

        .test {
            color: red;
        }

        html {
            background-color: #333333 !important;
        }

        /* Site Width */
        #wrapper, #main, #main.dark {
            background-color: #ffffff
        }

        .header-main {
            height: 58px
        }

        #logo img {
            max-height: 58px
        }

        #logo {
            width: 258px;
        }

        .header-bottom {
            min-height: 10px
        }

        .header-top {
            min-height: 20px
        }

        .transparent .header-main {
            height: 30px
        }

        .transparent #logo img {
            max-height: 30px
        }

        .has-transparent + .page-title:first-of-type, .has-transparent + #main > .page-title, .has-transparent + #main > div > .page-title, .has-transparent + #main .page-header-wrapper:first-of-type .page-title {
            padding-top: 30px;
        }

        .header.show-on-scroll, .stuck .header-main {
            height: 70px !important
        }

        .stuck #logo img {
            max-height: 70px !important
        }

        .header-bg-color, .header-wrapper {
            background-color: rgba(43, 43, 43, 0.56)
        }

        .header-bottom {
            background-color: #f1f1f1
        }

        @media (max-width: 549px) {
            .header-main {
                height: 70px
            }

            #logo img {
                max-height: 70px
            }
        }

        /* Color */
        .accordion-title.active, .has-icon-bg .icon .icon-inner, .logo a, .primary.is-underline, .primary.is-link, .badge-outline .badge-inner, .nav-outline > li.active > a, .nav-outline > li.active > a, .cart-icon strong, [data-color='primary'], .is-outline.primary {
            color: #82b14e;
        }

        /* Color !important */
        [data-text-color="primary"] {
            color: #82b14e !important;
        }

        /* Background */
        .scroll-to-bullets a, .featured-title, .label-new.menu-item > a:after, .nav-pagination > li > .current, .nav-pagination > li > span:hover, .nav-pagination > li > a:hover, .has-hover:hover .badge-outline .badge-inner, button[type="submit"], .button.wc-forward:not(.checkout):not(.checkout-button), .button.submit-button, .button.primary:not(.is-outline), .featured-table .title, .is-outline:hover, .has-icon:hover .icon-label, .nav-dropdown-bold .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold > li > a:hover, .nav-dropdown-bold.dark .nav-column li > a:hover, .nav-dropdown.nav-dropdown-bold.dark > li > a:hover, .is-outline:hover, .tagcloud a:hover, .grid-tools a, input[type='submit']:not(.is-form), .box-badge:hover .box-text, input.button.alt, .nav-box > li > a:hover, .nav-box > li.active > a, .nav-pills > li.active > a, .current-dropdown .cart-icon strong, .cart-icon:hover strong, .nav-line-bottom > li > a:before, .nav-line-grow > li > a:before, .nav-line > li > a:before, .banner, .header-top, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover, .button.primary:not(.is-outline), input[type='submit'].primary, input[type='submit'].primary, input[type='reset'].button, input[type='button'].primary, .badge-inner {
            background-color: #82b14e;
        }

        /* Border */
        .nav-vertical.nav-tabs > li.active > a, .scroll-to-bullets a.active, .nav-pagination > li > .current, .nav-pagination > li > span:hover, .nav-pagination > li > a:hover, .has-hover:hover .badge-outline .badge-inner, .accordion-title.active, .featured-table, .is-outline:hover, .tagcloud a:hover, blockquote, .has-border, .cart-icon strong:after, .cart-icon strong, .blockUI:before, .processing:before, .loading-spin, .slider-nav-circle .flickity-prev-next-button:hover svg, .slider-nav-circle .flickity-prev-next-button:hover .arrow, .primary.is-outline:hover {
            border-color: #82b14e
        }

        .nav-tabs > li.active > a {
            border-top-color: #82b14e
        }

        .widget_shopping_cart_content .blockUI.blockOverlay:before {
            border-left-color: #82b14e
        }

        .woocommerce-checkout-review-order .blockUI.blockOverlay:before {
            border-left-color: #82b14e
        }

        /* Fill */
        .slider .flickity-prev-next-button:hover svg, .slider .flickity-prev-next-button:hover .arrow {
            fill: #82b14e;
        }

        /* Background Color */
        [data-icon-label]:after, .secondary.is-underline:hover, .secondary.is-outline:hover, .icon-label, .button.secondary:not(.is-outline), .button.alt:not(.is-outline), .badge-inner.on-sale, .button.checkout, .single_add_to_cart_button {
            background-color: #399912;
        }

        /* Color */
        .secondary.is-underline, .secondary.is-link, .secondary.is-outline, .stars a.active, .star-rating:before, .woocommerce-page .star-rating:before, .star-rating span:before, .color-secondary {
            color: #399912
        }

        /* Color !important */
        [data-text-color="secondary"] {
            color: #399912 !important;
        }

        /* Border */
        .secondary.is-outline:hover {
            border-color: #399912
        }

        .success.is-underline:hover, .success.is-outline:hover, .success {
            background-color: #399912
        }

        .success-color, .success.is-link, .success.is-outline {
            color: #399912;
        }

        .success-border {
            border-color: #399912 !important;
        }

        body {
            font-size: 100%;
        }

        @media screen and (max-width: 549px) {
            body {
                font-size: 100%;
            }
        }

        body {
            font-family: "Open Sans", sans-serif
        }

        body {
            font-weight: 0
        }

        body {
            color: #000000
        }

        .nav > li > a {
            font-family: "Poppins", sans-serif;
        }

        .nav > li > a {
            font-weight: 700;
        }

        h1, h2, h3, h4, h5, h6, .heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a {
            font-family: "Open Sans", sans-serif;
        }

        h1, h2, h3, h4, h5, h6, .heading-font, .banner h1, .banner h2 {
            font-weight: 0;
        }

        h1, h2, h3, h4, h5, h6, .heading-font {
            color: #000000;
        }

        .alt-font {
            font-family: "Noto Sans", sans-serif;
        }

        .alt-font {
            /*font-weight: 0 !important;*/
        }

        .absolute-footer, html {
            background-color: #333333
        }

        .label-new.menu-item > a:after {
            content: "New";
        }

        .label-hot.menu-item > a:after {
            content: "Hot";
        }

        .label-sale.menu-item > a:after {
            content: "Sale";
        }

        .label-popular.menu-item > a:after {
            content: "Popular";
        }
    </style>
</head>