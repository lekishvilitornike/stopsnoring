<header id="header" class="header has-sticky sticky-jump">
    <div class="header-wrapper">
        <div id="masthead" class="header-main show-logo-center nav-dark">
            <div class="header-inner flex-row container logo-center medium-logo-center" role="navigation">

                <!--Logo -->
                <div id="logo" class="flex-col logo">
                    <!--Header logo-->
                    <a href="https://example.com/" title="SilentSnore ® - Official Website"
                       rel="home">
                        SilentSnore ® </a>
                </div>

                <!--Mobile Left Elements-->
                <div class="flex-col show-for-medium flex-left">
                    <ul class="mobile-nav nav nav-left ">
                        <li class="nav-icon has-icon">
                            <a href="#" data-open="#main-menu" data-pos="left" data-bg="main-menu-overlay"
                               data-color="" class="is-small" aria-controls="main-menu" aria-expanded="false">

                                <i class="icon-menu"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <!--Left Elements-->
                <div class="flex-col hide-for-medium flex-left
            ">
                    <ul class="header-nav header-nav-main nav nav-left  nav-uppercase">
                    </ul>
                </div>

                <!--Right Elements-->
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="header-nav header-nav-main nav nav-right  nav-uppercase">
                    </ul>
                </div>

                <!--Mobile Right Elements-->
                <div class="flex-col show-for-medium flex-right">
                    <ul class="mobile-nav nav nav-right ">
                    </ul>
                </div>

            </div><!-- .header - inner-->

            <!--Header divider-->
            <div class="container">
                <div class="top-divider full-width"></div>
            </div>
        </div><!-- .header - main-->
        <div class="header-bg-container fill">
            <div class="header-bg-image fill"></div>
            <div class="header-bg-color fill"></div>
        </div><!-- .header - bg - container-->   </div><!--header-wrapper-->
</header>
