<footer id="footer" class="footer-wrapper">


    <!--FOOTER 1-->


    <!--FOOTER 2-->


    <div class="absolute-footer dark medium-text-center text-center">
        <div class="container clearfix">

            <div class="footer-secondary pull-right">
                <div class="footer-text inline-block small-block">
                    *DISCLAIMER: The information on this page is informative and in no way supersedes the
                    opinion of
                    the experts . The results may vary depending on the particularities of each body organism .
                    Please
                    consult a specialist before using the product . By clicking the "order" button you confirm
                    you
                    have read < strong><a href="./privacyPolicy.php"> Privacy Policy </a></strong > and give
                    your
                    consent
                    to the processing of your personal data .
                </div>
            </div><!-- -right-->

            <div class="footer-primary pull-left">
                <div class="menu-bottommenu-container">
                    <ul id="menu-bottommenu" class="links footer-nav uppercase">
                        <li id="menu-item-623"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-623"><a
                                href="aboutUs.php"> About Us </a></li>
                        <li id="menu-item-147"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-147"><a
                                href="termConditions.php"> Terms Of
                                Business </a>
                        </li>
                        <li id="menu-item-196"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-196"><a
                                href="privacyPolicy.php"> Privacy
                                Policy </a></li>
                        <li id="menu-item-192"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-192"><a
                                href="cookiePolicy.php"> Cookie Policy </a>
                        </li>
                        <li id="menu-item-146"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-146"><a
                                href="contactUs.php"> Contact Us </a></li>
                    </ul>
                </div>
                <div class="copyright-footer">
                    2019 © All Rights Reserved . <br><strong> Silent - Snore . shop</strong></div>
            </div><!-- .left-->
        </div><!-- .container-->
    </div><!-- .absolute - footer-->

</footer><!-- .footer - wrapper-->
