<!--Mobile Sidebar-->
<div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
    <div class="sidebar-menu no-scrollbar ">
        <ul class="nav nav-sidebar  nav-vertical nav-uppercase">
            <li class="header-search-form search-form html relative has-icon">
                <div class="header-search-form-wrapper">
                    <div class="searchform-wrapper ux-search-box relative form- is-normal">
                        <form method="get" class="searchform" action="https://stopsnoring.silent-snore.shop/"
                              role="search">
                            <div class="flex-row relative">
                                <div class="flex-col flex-grow">
                                    <input type="search" class="search-field mb-0" name="s" value="" id="s"
                                           placeholder="Search&hellip;"/>
                                </div><!-- .flex - col-->
                                <div class="flex-col">
                                    <button type="submit"
                                            class="ux-search-submit submit-button secondary button icon mb-0">
                                        <i class="icon-search"></i></button>
                                </div><!-- .flex - col-->
                            </div><!-- .flex - row-->
                            <div class="live-search-results text-left z-top"></div>
                        </form>
                    </div>
                </div>
            </li>
            <li>
                <a href="https://example.com?url={value}">
                    Assign
                    a menu in Theme Options > Menus</a></li>
            WooCommerce not Found
        </ul>
    </div><!--inner -->
</div><!-- #mobile-menu -->
<div id="wpfomo">
    <div class="wpfomo-product-thumb-container"><img src="" class="wpfomo-product-thumb"></div>
    <div class="wpfomo-content-wrapper"><p><span class="wpfomo-buyer-name"></span></p><a href="#" target="_blank"
                                                                                         class="wpfomo-product-name"></a>
    </div>
</div>