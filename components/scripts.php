
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/stopsnoring.silent-snore.shop\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {
            "messages": {
                "empty": "Please verify that you are not a robot."
            }
        }, "cached": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='./assets/js/scripts.js?ver=4.9'></script>
<script type='text/javascript'
        src='./assets/js/hoverIntent.min.js?ver=1.8.1'></script>
<script type='text/javascript'
        src='./assets/js/flatsome.js?ver=3.6.2'></script>
<script type='text/javascript'
        src='./assets/js/flatsome-wp-rocket.js?ver=3'></script>
<script type='text/javascript'
        src='./assets/js/flatsome-live-search.js?ver=3.6.2'></script>